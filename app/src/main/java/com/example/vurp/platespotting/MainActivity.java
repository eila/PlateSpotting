package com.example.vurp.platespotting;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SpotHandler mSpotHandler;

    private int nextSpot;

    private Button newSpotButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        mSpotHandler = new SpotHandler(this);

        createListeners();

    }

    @Override
    protected void onStart() {
        super.onStart();
        populateList();

        newSpotButton = (Button)findViewById(R.id.new_spot);
        newSpotButton.setText(getString(R.string.new_spot) + ":\t" + (mSpotHandler.getNextSpotTitle()));
    }

    private void populateList(){
        ListView listSpots = (ListView) findViewById(R.id.spots);
        // TODO: Show two rows
        ArrayAdapter listAdapter = new ArrayAdapter<Spot>(
                this,
                android.R.layout.simple_list_item_1,
                mSpotHandler.getSpots()
                );
        listSpots.setAdapter(listAdapter);
    }

    private void createListeners() {
        // Switches to mapview
        Button switchButton = (Button) findViewById(R.id.map_switch);
        switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,
                        MapsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // adds a new spot
        Button newButton = (Button) findViewById(R.id.new_spot);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextSpot = mSpotHandler.createNextSpot().getNumber();
                Intent intent = new Intent(MainActivity.this,
                        DetailActivity.class);
                intent.putExtra("spot", nextSpot);
                startActivity(intent);
            }
        });

        // views the selected spot
        AdapterView.OnItemClickListener itemClickListener =
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> listView,
                                            View v,
                                            int position,
                                            long id) {
                        Intent intent = new Intent(MainActivity.this,
                                DetailActivity.class);

                        // HACK: Doesn't use values from the item
                        intent.putExtra("spot", position);
                        startActivity(intent);
                    }
                };
        //Add the listener to the list view
        ListView listView = (ListView) findViewById(R.id.spots);
        listView.setOnItemClickListener(itemClickListener);
    }

}
