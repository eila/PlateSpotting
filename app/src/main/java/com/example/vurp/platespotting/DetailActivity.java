package com.example.vurp.platespotting;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.List;

public class DetailActivity extends AppCompatActivity {
    SpotHandler mSpotHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mSpotHandler = new SpotHandler(this);
        populateSpot(getIntent().getIntExtra("spot", -1));
    }

    /**
     * Populates the view with the spot of the given key
     * @param key to populate the page with
     */
    private void populateSpot(int key){
        Spot spot = mSpotHandler.getSpot(key);

        if (spot != null){
            //Populate the spot name
            TextView title = (TextView)findViewById(R.id.spotTitle);
            title.setText(spot.getTitle());

            //Populate the spot date
            TextView date = (TextView)findViewById(R.id.spotDate);
            date.setText(spot.getDate());

            //Populate the spot time
            TextView time = (TextView)findViewById(R.id.spotTime);
            time.setText(spot.getTime());

            String spotLat = "Okänd";
            String spotLng = "Okänd";
            if (spot.getLocation() != null){
                spotLat = Double.toString(spot.getLocation().getLatitude());
                spotLng = Double.toString(spot.getLocation().getLongitude());
            }

            //Populate the spot latitude
            TextView lat = (TextView)findViewById(R.id.spotLat);
            lat.setText(spotLat);

            //Populate the spot longitude
            TextView lng = (TextView)findViewById(R.id.spotLng);
            lng.setText(spotLng);
        }
        else{
            Log.e("DetailActivity", "No spot found");

            TextView name = (TextView)findViewById(R.id.spotTitle);
            name.setText(R.string.error);
        }
    }
}
