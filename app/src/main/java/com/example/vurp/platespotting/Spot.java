package com.example.vurp.platespotting;

import android.location.Location;
import android.support.annotation.Nullable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Represents a spot found by the user.
 */

public class Spot {
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private final static DateFormat TIME_FORMAT = new SimpleDateFormat("hh:mm:ss");

    private int mNumber;
    private Location mLocation;
    private Date mDate;

    /**
     * creates a new spot with a given number
     *
     * @param number the number it should be
     * @param location the location the spot was found at
     */
    public Spot(int number, @Nullable Location location) {
        this.mNumber = number;
        this.mLocation = location;
        this.mDate = new Date();
    }

    /**
     * Returns the title/ number of this spot as an string with a length of 3.
     *
     * @return title as a string
     */
    public String getTitle(){
        String numberString;
        numberString = String.valueOf(getNumber());
        while (numberString.length() < 3){
            numberString = "0" + numberString;
        }
        return numberString;
    }

    /**
     * Returns the title/ number of this spot as an integer
     *
     * @return number
     */
    public int getNumber() {
        return mNumber;
    }

    /**
     * Returns the current location. Warning: May be null.
     *
     * @return the location. Warning: May be null
     */
    public @Nullable Location getLocation (){
        return mLocation;
    }

    /**
     * Returns dateTime of this object
     *
     * @return this things date
     */
    public Date getDateTime() {
        return mDate;
    }

    /**
     * Returns Date formatted as yyyy-mm-dd.
     *
     * @return date as string
     */
    public String getDate() {
        return DATE_FORMAT.format(mDate);
    }

    /**
     * Returns Time formatted as hh:mm:ss.
     *
     * @return Time as string
     */
    public String getTime() {
        return TIME_FORMAT.format(mDate);
    }

    @Override
    public String toString(){
        return getTitle() + ":\tDen " + getDate() + " vid " + getTime();
    }


}


