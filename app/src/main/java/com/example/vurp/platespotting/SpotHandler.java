package com.example.vurp.platespotting;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Handles saving and retrieving spots. Shared Preferences are used for storage.
 *
 * @author Einar Largenius
 */

class SpotHandler {
    private Gson gson = new Gson();
    private Context mContext;
    private SharedPreferences mPrefs;

    LocationManager locationManager;

    private Type listType;
    private ArrayList<Spot> spots;
    private static Location lastLocation = null;
    /**
     * Creates a new SpotHandler relating to the given context.
     *
     * @param context the context to manage SpotData from
     */
    public SpotHandler(Context context) {
        this.mContext = context;
        mPrefs = mContext.getSharedPreferences("PlateSpotting", 0);
        listType = new TypeToken<ArrayList<Spot>>() {
        }.getType();

        loadSpots();
    }

    /**
     * Creates a new spot and adds it to the list. Number, Location, date and time are handled
     * automatically. If a location cannot be found it is null.
     *
     * @return the created spot.
     */
    public Spot createNextSpot() {
        Location location;

        Spot newSpot;

        //TODO: fix location
        locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ) {
            location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            Toast.makeText(mContext, "Ingen plats tillgänglig", Toast.LENGTH_LONG).show();
            newSpot = new Spot(getNextSpot(), location);
        } else {
            Toast.makeText(mContext, "Ingen plats tillgänglig", Toast.LENGTH_LONG).show();
            newSpot = new Spot(getNextSpot(), null);
        }

        spots.add(newSpot);
        Log.v("SpotHandler", "added spot:\t" + newSpot);
        setSpots(spots);
        return newSpot;
    }

    /**
     * Returns a given spot
     *
     * @param number the number of the spot that is wanted
     * @return the given spot if found, null if not found
     */
    public @Nullable Spot getSpot(int number){
        for (Spot item : spots) {
            if (item.getNumber() == number) return item;
        }
        return null; }

    /**
     * Loads the spots
     */
    private void loadSpots() {

        try {
            String inString = mPrefs.getString("spots", null);

            if (inString != null )Log.v("SpotHandler", inString);

            spots = gson.fromJson(inString, listType);

        } catch (ClassCastException | JsonSyntaxException e) {
            Log.e("SpotHandler", e.getMessage());
        }

        if (spots == null) {
            Log.i("SpotHandler", "No spots saved, creating new list");
            spots = new ArrayList<Spot>();
        }
    }

    /**
     * retrieves all spots.
     *
     * @return the spots
     */
    public ArrayList<Spot> getSpots() {
        return spots;
    }

    private void setSpots(ArrayList<Spot> spots) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString("spots", gson.toJson(spots));

        // Note: not asynchronous because I want to be sure it really gets saved.
        editor.commit();
    }

    /**
     * Returns the next spot to be found as an int.
     *
     * @return the next spot
     */
    private int getNextSpot() {
        return spots.size();
    }

    /**
     * Returns the next spot to be found as a string.
     *
     * @return the next spot
     */
    public String getNextSpotTitle(){
        String number = String.valueOf(spots.size());
        while (number.length() < 3)   {
            number = "0" + number;
        }
        return number;
    }
}
