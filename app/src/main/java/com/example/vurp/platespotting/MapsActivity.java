package com.example.vurp.platespotting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private SpotHandler mSpotHandler;

    private int nextSpot;

    private Button newSpotButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mSpotHandler = new SpotHandler(this);

        createListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        newSpotButton = (Button)findViewById(R.id.new_spot);
        newSpotButton.setText(getString(R.string.new_spot) + ":\t" + (mSpotHandler.getNextSpotTitle()));
    }

    /**
     * Creates listeners for buttons
     */
    private void createListeners() {
        // Switches to listview
        Button switchButton = (Button) findViewById(R.id.list_switch);
        switchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MapsActivity.this,
                        MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // Adds a new spot
        Button newButton = (Button) findViewById(R.id.new_spot);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextSpot = mSpotHandler.createNextSpot().getNumber();
                Intent intent = new Intent(MapsActivity.this,
                        DetailActivity.class);
                intent.putExtra("spot", nextSpot);
                startActivity(intent);
            }
        });
    }

    /**
     * Manipulates the map when it's available.
     * The API invokes this callback when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user receives a prompt to install
     * Play services inside the SupportMapFragment. The API invokes this method after the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        List<Spot> spots = mSpotHandler.getSpots();
        LatLng visby = new LatLng(57.634800, 18.294840);
        for (Spot item : spots ) {
            if(item.getLocation() != null){
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(item.getLocation().getLatitude(), item.getLocation().getLongitude()))
                    .title(item.getTitle()));
            }
        }

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener(){
            @Override
            public boolean onMarkerClick(Marker marker) {
                int spotNumber = Integer.parseInt(marker.getTitle());
                Intent intent = new Intent(MapsActivity.this, DetailActivity.class);
                intent.putExtra("spot", spotNumber);
                startActivity(intent);
                return true;
            }
        });
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(visby));
    }
}
